#!/bin/bash
echo -n "picoCTF{"
wget -qO- https://2019shell1.picoctf.com/static/b6f6a6f9086d2f7d6ebabc9dc0fddc4b/VaultDoor1.java | grep "password\\.charAt" | awk -F'[()\x27]' '{print $2 " " $4}' | sort -g | awk ' {print $2}' | tr -d '\n'
echo "}"
