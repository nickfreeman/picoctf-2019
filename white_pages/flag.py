#!/usr/bin/env python
inp = input()
out = ""
try:
	for i in range(0, len(inp), 2):
		out = out + '{:x}'.format(int(inp[i:i+2], 4))
except ValueError: 
	pass
print(out)
