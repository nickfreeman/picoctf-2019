#!/usr/bin/env python
test = "16 9 3 15 3 20 6 { 20 8 5 14 21 13 2 5 18 19 13 1 19 15 14 }"
def num(s):
    try:
        return chr(int(s)+ord('A')-1)
    except ValueError:
        return s

for char in test.split(' '):
	print(num(char), end='')
