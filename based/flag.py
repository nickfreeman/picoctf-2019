#!/usr/bin/env python
while True:
	inp = input()
	for n in [2,8]:
		try:
			for e in inp.split(' '):
				print(chr(int(e, n)), end='')
			print()
		except ValueError: 
			pass
	try:
		print(bytes.fromhex(inp).decode("utf-8"))
	except ValueError: 
		pass
