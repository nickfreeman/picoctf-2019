#!/usr/bin/env python
text="bqnrrhmfsgdqtahbnmxjvkipdb"
for i in range(0, 25):
	print("picoCTF{", end='')
	for char in text:
		print(chr((ord(char)-ord('a')+i)%26+ord('a')), end='')
	print("}")
