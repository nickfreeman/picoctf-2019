#!/usr/bin/env python
enc = "cvpbPGS{abg_gbb_onq_bs_n_ceboyrz}"
for c in enc:
	if c == '{' or c == '}' or c == '_':
		print(c, end='')
	elif (ord(c)>=ord('a') and ord(c)<ord('m')) or (ord(c)>=ord('A') and ord(c)<ord('M')):
		print(chr(ord(c)+13), end='')
	else:
		print(chr(ord(c)-13), end='')
