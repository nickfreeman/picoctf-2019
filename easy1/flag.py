#!/usr/bin/env python
flag = "UFJKXQZQUNB"
key = "SOLVECRYPTO"
if len(key) != len(flag):
	raise ValueError('Wrong length!')

print("picoCTF{", end='')
for i in range(0, len(key)):
	print(chr((ord(flag[i]) - ord(key[i]) - ord('A')*2)%26+ord('A')), end='')
print("}")
