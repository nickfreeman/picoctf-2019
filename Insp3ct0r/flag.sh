#!/bin/bash
BASE_URL=https://2019shell1.picoctf.com/problem/41474
wget -qO- $BASE_URL/ | grep flag
wget -qO- $BASE_URL/mycss.css | grep flag
wget -qO- $BASE_URL/myjs.js | grep flag
