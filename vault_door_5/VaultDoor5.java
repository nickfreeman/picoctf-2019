import java.util.*;

class VaultDoor5 {
    public static void main(String args[]) {
        String expected = "JTYzJTMwJTZlJTc2JTMzJTcyJTc0JTMxJTZlJTY3JTVm"
                        + "JTY2JTcyJTMwJTZkJTVmJTYyJTYxJTM1JTY1JTVmJTM2"
                        + "JTM0JTVmJTMwJTY2JTMzJTMwJTM5JTY0JTM0JTMw";
        String decode = new String(Base64.getDecoder().decode(expected));
        System.out.println(decode);
        byte[] out = new byte[decode.length()/3];
        for(int i = 0; i < decode.length(); i+=3) {
        	out[i/3] = Byte.parseByte(decode.substring(i+1, i+3), 16);
        }
        System.out.println(new String(out));
    }
}
