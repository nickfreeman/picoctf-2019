class VaultDoor3 {
    public static void main(String args[]) {
    	String password = "jU5t_a_sna_3lpm13gc49_u_4_m0rf41";
        char[] buffer = new char[32];
        int i;
        for (i=0; i<8; i++) {
            buffer[i] = password.charAt(i);
        }
        for (; i<16; i++) {
            buffer[23-i] = password.charAt(i);
        }
        for (; i<32; i+=2) {
            buffer[46-i] = password.charAt(i);
        }
        for (i=31; i>=17; i-=2) {
            buffer[i] = password.charAt(i);
        }
        String s = new String(buffer);
        System.out.println(s);
    }
}
